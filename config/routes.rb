Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get "/", to: "pages#index", as: "home"
  # articles
  get "/posts", as: "posts_format", to: "posts#index"
  get "/post/:id", to: "posts#show"
  get "/article/:slug", to: "pages#show", as: "post_show"
  # comments
  post "/comment/new", to: "comments#create", as: "create_comment"
  get "/comment/:id/delete", as: "delete_comment", to: "comments#destroy"
  #user
  get "/user/:username", to: "users#show", as: "show_user"
  get "/connexion", to: "connect#index_login", as: "login"
  post "/connexion", to: "connect#login"
  #categories
  get "categorie/:name", to: "categories#show", as: "show_category"
  #others
  post "/nous-contactez", to: "contact#index", as: "footer_mail"
  get "/404", :to => "pages#index"
  get "/recherche", as: "search", to: "pages#search"
  get "/rss", as: 'rss', to: "rss#index"

  # administration
  scope "/admin" do
    get "/", as: "admin_home", to: "admin#index"
    post "/", as: "new_post", to: "posts#create"
    get "/commentaires", as: "comments_management", to: "admin#manage_comments"
    get "/all_comments", as: "all_comments", to: "admin#all_comments"
    get "/ninja/:id", as: "ninja", to: "admin#ninja"
    get "/users", as: "users_management", to: "admin#manage_users"
    get "/categories", as: "categories_management", to: "admin#manage_categories"
    get "/categories/:id/destroy", as: "categories_destroy", to: "categories#destroy"
  end

  # user profile
  scope "/profil" do
    get "/", as: "user_profile", to: "profiles#dashboard"
    get "/deconextion", as: "disconnect", to: "profiles#disconnect"
    get "/article/:id/edit", as: "edit_article", to: "profiles#edit_post"
    post "/article/edit/:id", as: "edit", to: "posts#edit"
    get "/article/:id/delete", as: "delete", to: "posts#destroy"
  end

end
