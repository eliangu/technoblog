=begin
* Controlleur gérant la gestion du profile de l'utilisateur
=end

class ProfilesController < ApplicationController

  include AdminHelper
  before_action :protection

  def dashboard
    render layout: "layouts/main"
  end

  def edit_post
    user = User.find(session[:id])
    post = Post.find(params[:id])
    if post && user && is_connect_and_admin?
      if post.user_id == user.id
        @post = post
        render :layout => "layouts/main"
      else
        redirect_to home_path
      end
    else
      redirect_to home_path
    end
  end

  def disconnect
    reset_session
    flash[:msg] = "Vous avez bien été déconnecté"
    redirect_to admin_home_path
  end

  private
  def protection
    if is_connect?
      @user = User.find(session[:id])
    else
      redirect_to home_path
    end
  end

end