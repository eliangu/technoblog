=begin

* Classe gérant les commentaires

=end

class CommentsController < ApplicationController
  skip_before_action :verify_authenticity_token

  include AdminHelper

  def create
    # véfication di l'utilisateur est connecté
    if is_connect?
      # recupération des requètes
      content = params[:content]
      id_post = params[:post]
      post = Post.find(id_post)
     # vérification pour voir si l'article est bien valide
      if post
        if Post.find(id_post).is_posted
          # instance de la clase Comment
          comment = Comment.new
          # ajout de l'id de l'utilisateur
          comment.id_user = session[:id]
          # ajout du contenu
          comment.content = content
          # ajout de l'id du post
          comment.id_post = id_post
          # ajout du commentaire
          comment.save
          # redirection lorsque tout a bien été ajouté
          redirect_to(post_show_path(Post.find(id_post).slug))
        else
          redirect_to home_path
        end
      else
        redirect_to home_path
      end
    else
      # redirection si il n'est pas connecté
      redirect_to :back
    end
  end

  def destroy
    referal_link = params[:redirect]
    id = params[:id]
    Comment.find(id).destroy
    flash[:msg] = "Le commentaire a bien été supprimé !"
    redirect_to referal_link
  end

end