=begin
* Controlleur gérant les différents formats pour récupérer les articles ou les commentaires
=end

class PostsController < ApplicationController
  skip_before_action :verify_authenticity_token
  # verification si l'utilisateur est connté et est administrateur
  before_action :validate_user, :only => ["create", "edit", "destroy"]
  include AdminHelper
  def index
    respond_to do |format|
      format.json do
        render :json => Post.all.limit(4).to_json(:include => :user)
      end
    end
  end

  def show
    respond_to do |format|
      format.json do
        render :json => Post.find(params[:id])
      end
    end
  end

  def create
    post = Post.new
    post.title = params[:title]
    post.content = params[:content]
    post.slug = params[:slug]
    post.user_id = session[:id]
    post.category_id = params[:category]
    post.thumbnail = params[:thumbnail]
    if post.save

      # flash message
      flash[:msg] = "L'article a bien été créer !"
      # redirecting
      redirect_to post_show_path(params[:slug])
    else

      flash[:error] = post.errors.full_messages
      redirect_to admin_home_path

    end

  end

  def edit
    post = Post.find(params[:id])
    user = User.find(session[:id])
    if post.user_id == user.id && user && post
      post.title = params[:title]
      post.content = params[:content]
      post.slug = params[:slug]
      post.user_id = session[:id]
      post.category_id = params[:category]
      post.thumbnail = params[:thumbnail]
      if params[:is_posted] == "on"
        post.posted = 0
      else
        post.posted = 1
      end
      post.save
      # flash message
      flash[:msg] = "L'article a bien été modifié !"
      # redirecting
      if post.is_posted
        redirect_to post_show_path(params[:slug])
      else
        redirect_to user_profile_path
      end
    else
      redirect_to home_path
    end
  end

  def destroy
    post = Post.find(params[:id])
    user = User.find(session[:id])
    if post.user_id == user.id && user && post
      post.destroy
      # flash message
      flash[:msg] = "L'article a bien été supprimé !"
      # redirecting
      redirect_to user_profile_path
    else
      redirect_to home_path
    end
  end

  private
  def validate_user
    # redirection si l'utilisateur n'est pas administrateur
    redirect_to home_path unless is_connect_and_admin?
  end

end