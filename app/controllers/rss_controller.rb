=begin
* Controlleur gérant le flux rss
=end

class RssController < ApplicationController
  def index
    @posts = Post.posted.limit(25)
    render :layout => false
  end
end