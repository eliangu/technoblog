class UsersController < ApplicationController

  before_action :get_all_posts

  def show
    @user = User.find_by(username: params[:username])
    render "users/show"
  end

  private
  def get_all_posts
    @posts = Post.all.order("created_at DESC")
  end

end
