class PagesController < ApplicationController

  before_action :get_all_posts, :only => ["show", "search"]

  def index
    @posts = Post.posted.order("RAND()")
    render "pages/index"
  end

  def show
    @post = Post.find_by(slug: params[:slug])
    @next = Post.posted.where("id > ?", @post.id).first
    @last = Post.posted.where("id < ?", @post.id).last
    render "pages/show"
  end

  def search
    @q = params[:q]
    if @q
      @post = Post.posted.where("lower(content) LIKE :content OR lower(title) LIKE :title OR lower(slug) LIKE :slug", {
          :content => "%#{@q.downcase}%",
          :title => "%#{@q.downcase}%",
          :slug => "%#{@q.downcase}%"
      })
    else
      @post = Post.posted.order("created_at ASC")
    end
    render "pages/search"
  end

  private
  def get_all_posts
    @posts = Post.order("RAND()")
  end

end
