=begin
* Controlleur gérant le système de connexion
=end
class ConnectController < ApplicationController

  # modules includes
  include AdminHelper
  include Hashable
  # =============== end ===================
  skip_before_action :verify_authenticity_token

  def index_login
    render "connect/login"
  end

  def login
    params.permit(:email, :password)
    if !is_connect?
      password = params[:password]
      email = params[:email]
      user = User.where("email = :email AND password = :password", {email: email, password: hash(password)}).first
      if user
        session[:id] = user.id
        flash[:msg] = "Vous été maintenant connecté !"
        redirect_to home_path
      else
        flash[:msg] = "Le compte n'a pas été trouvé !"
        redirect_to login_path
      end
    else
      redirect_to home_path
    end
  end

end