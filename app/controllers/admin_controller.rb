=begin
* Controlleur gérant l'interface d'administration
=end

class AdminController < ApplicationController
  # verification si l'utilisateur est connté et est administrateur
  before_action :validate_user
  before_action :is_super_admin, :only => ["manage_comments", "manage_users", "manage_categories"]
  include AdminHelper

  def index
    render :layout => "layouts/main"
  end

  def manage_comments
    @comments = Comment.all
    render :layout => "layouts/main"
  end

  def all_comments
    @comments = Comment.all.order("created_at DESC")
    render :json => @comments.to_json
  end

  def ninja
    id_user = params[:id]
    user = User.find(id_user)
    if user && is_connect_and_superadmin? && user.id != session[:id]
      reset_session
      session[:id] = user.id
      redirect_to home_path
    else
      redirect_to user_profile_path
    end
  end

  def manage_users
      @users = User.order("created_at DESC")
      render :layout => "layouts/main"
  end

  def manage_categories

    @categories = Category.order("id DESC")
    render :layout => "layouts/main"

  end


  private
  # Fonction servant à savoir si l'utilisateur est administrateur et donc a le droit d'accéder à la page
  def validate_user
    # redirection si l'utilisateur n'est pas administrateur
    redirect_to home_path unless is_connect_and_admin?
  end
  # Fonction servant à savoir si l'utilisateur est super admin
  def is_super_admin
    redirect_to home_path unless is_connect_and_superadmin?
  end

end