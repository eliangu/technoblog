=begin
* Controlleur gérant le formulaire de contact dans le footer du site
=end

class ContactController < ApplicationController
  skip_before_action :verify_authenticity_token
  def index()
    redirect_to home_url, msg: "Le commentaires a bien été posté"
  end
end