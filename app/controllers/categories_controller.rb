=begin
* Controlleur servant à gérer l'affichage des catégories
=end

class CategoriesController < ApplicationController

  before_action :has_category, :only => ["destroy"]

  # @return [Object]
  # Méthode permettant de récupérer toutes les catégories et de les envoyer à la vue
  def show
    @category = Category.find_by(name: params[:name])
    render "pages/category"
  end

  # methode permettant de supprimer une catégorie
  def destroy

  end

  private
  # methode permettant de savoir si tous les paramètres passés en GET sont présents
  def has_category

    id = params[:id]
    referer = params[:redirect] ? params[:redirect] : home_path
    category = Category.find(id)

    if id && category
      category.destroy
      Post.where("category_id", category.id).destroy_all
      redirect_to referer
    else
      flash[:error] = "La catégorie ne semble pas être valide..."
      redirect_to categories_management_path
    end

  end

end