module Hashable
  # @param [Object] value
  # @return [String] value
  def hash(value)
    return Digest::SHA1.hexdigest(value)
  end
end