module AdminHelper

  # @return [Boolean]
  # Fonction servant à savoir si l'utilisateur est connecté
  def is_connect?
    # recupération de la session
    id = session[:id]
    # vérification si la session existe
    if id
      # recuperation de l'utilisateur
      user = User.find(id)
      # vérifiction si l'utilisateur existe
      if user
        # return true si l'utilisateur existe
          return true
      else
        # return false si l'utilisatuer n'existe pas
        return false
      end
    else
      # return false si la session n'existe pas
      return false
    end
  end

  # @return [Boolean]
  # fonction servant a savoir si l'utiisateur est connecté et est administrateur du site
  def is_connect_and_admin?
    # verification si l'utilisateur est connecté
    if is_connect?
      # recupération de l'utiliateur
      user = User.find(session[:id])
      # vérification si l'utilisateur est valide
      if user
        # vérification si l'utilisateur est administrateur
        if user.level >= 2
          # return true si l'utilisateur est administrateur
          return true
        else
          # return false si l'utilisateur n'est pas administrateur
          return false
        end
      else
        # return false si l'utilisateur n'est pas valide
        return false
      end
    else
      # return false si l'utilisateur n'est pas connecté
      return false
    end
  end

  # @return [Object]
  # Fonction servant à tester si l'utilisateur est super admin
  def is_connect_and_superadmin?
    # verification si l'utilisateur est connecté
    if is_connect?
      # recupération de l'utiliateur
      user = User.find(session[:id])
      # vérification si l'utilisateur est valide
      if user
        # vérification si l'utilisateur est superadmin
        if user.level == 3
          # return true si l'utilisateur est superadmin
          return true
        else
          # return false si l'utilisateur n'est pas superadmin
          return false
        end
      else
        # return false si l'utilisateur n'est pas valide
        return false
      end
    else
      # return false si l'utilisateur n'est pas connecté
      return false
    end
  end

  # @return [String]
  # Fonction servant à récupérer le level name de l'utilisateur
  def get_level_name(id)
    # recupéraion de l'utilisateur
    user = User.find(id)
    # return "L'utilisateur n'existe pas" si l'utilisateur n'existe pas
    return "L'utilisateur n'existe pas" unless user

    if user.level == 1
      return "Membre"
    elsif user.level == 2
      return "Administrateur"
    end

  end
  
  # fonction servant à récupéré l'utilisateur connecté
  # @return [Object]
  def current_user
    if is_connect?
      id = session[:id]
      user = User.find(id)
      return user
    end
  end


  # fonction servant à savoir si l'utilisateur est bannis
  def is_banned?

    if is_connect?
      id = session[:id]
      user = User.find(id)
      return user.banned == 1
    end

  end

end