// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require_tree .
//= require_self
    var $mt = 0;

Turbolinks.setProgressBarDelay(10);
$(document).on("submit", "form[data-turboform]", function(e) {
    Turbolinks.visit(this.action+(this.action.indexOf('?') == -1 ? '?' : '&')+$(this).serialize());
    return false;
});

$("#search-text").on("keyup", function(e) {
    Turbolinks.visit($("#search-form").attr("action")+($("#search-form").attr("action").indexOf('?') == -1 ? '?' : '&')+$("#search-form").serialize());
    return false;
});

$( document ).on('turbolinks:load', function() {
    $("#comment-form").fadeOut(1);

    var $loaderPosts = $("#loader-posts");
    var $postContainer = $("#post-all");
    var $allPostList = $("#post-content-list");
    if (window.location.href == $postContainer.data("url") || window.location.href == $postContainer.data("url") + "#") {
        $loaderPosts.delay(1000).animate({
            opacity: 0
        }, 300, function () {
            $loaderPosts.addClass("loaded");
            $allPostList.addClass("loaded");
            $allPostList.animate({
                opacity: 1
            }, 300)
        });
    }

    $mt += 1;
    console.log($mt);



    // initialisation of the markdown textarea
        if($mt == 4) {
            $mt = 0;
            var $url = $(location).attr("href");
            if ($(location).attr("href") == "http://0.0.0.0:3000/admin" || $url.search("/profil")) {
                var simplemde = new SimpleMDE({
                    element: $("#markdown-textarea")[0],
                    spellChecker: false
                });
            }
        }


        $("#comment-button").on("click", function () {
           $("#comment-button").html("Chargement..");
           $("#comment-button").css({
               "opacity": 0
           });
        });

    $('code').each(function(i, block) {
        hljs.highlightBlock(block);
    });

    // comments form animation
    var $commentBtn = $("#comment-button")
    var $commentForm = $("#comment-form");

    $commentBtn.on("click", function () {
       $commentBtn.fadeOut(300);
       $commentForm.fadeIn(300);
    });

    var sticky = new Sticky('#sidebar');


});

$(window).ready(function () {
    var $url = $("#comments").data("url");
    console.log($url);
    var $container = $("#comments");
    getComments();

    function getComments() {

        $.getJSON($url, function (data) {
        var $html = "";
        data.forEach(function (value) {
            var $created = new Date(value.created_at);
            $html += "<tr><th>" + jQuery.trim(value.content).substring(0, 50)
                .split(" ").slice(0, -1).join(" ") + "..." + "</th><th>" + $created.getDate() + "/" + $created.getMonth() + "/" + $created.getFullYear() + "</th><th><a data-turbolinks=\"false\" href=\"/comment/" + value.id + "/delete?redirect=" + $(location).attr("href") + "\" class='btn btn-danger'>Supprimer</a></th></tr>";
        });
        setTimeout(function () {
            $container.animate({
                opacity: 0
            }, 300, function () {
                $container.html($html);
                $container.animate({
                    opacity: 1
                }, 300)
            })
        }, 1000)
    });
    setTimeout(function () {
        $("#commentSuccess").fadeOut(500);
    }, 4000)

    }

    $("#refresh-comment").on("click", function(){
        getComments();
    })

});

