=begin
* Model gérant tous les articles
=end


class Post < ApplicationRecord

  belongs_to :category, :class_name => "Category", :foreign_key => "category_id"
  belongs_to :user, :class_name => "User", :foreign_key => "user_id"
  has_many :comment, :class_name => "Comment", :foreign_key => "id_post"

  self.table_name = "posts"
  # validation
  validates :title, :presence => { :message => " ne devrait pas être vide" }
  validates :content, :presence => { :message => " ne devrait pas être vide" }
  validates :user_id, :presence => { :message => " ne devrait pas être vide" }
  validates :category_id, :presence => { :message => " ne devrait pas être vide" }
  validates :slug, :presence => { :message => " ne devrait pas être vide" }
  validates :thumbnail, :presence => { :message => " ne devrait pas être vide" }

  # ======================================================================
  # * Fonctions servant à recupérent le contenu de l'article avec un extrait
  # @return [Object]
  # @param int
  def getContent()
    return GitHub::Markdown.render_gfm(self.content).html_safe
  end
  # end getContent fonction
  # ======================================================================

  # ======================================================================
  # @return [Object]
  # Fonction servant à récupérer tous les articles postés
  def self.posted
    Post.where("posted", 1)
  end

  def is_posted
    return self.posted == 1 ? true : false
  end

end