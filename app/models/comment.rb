=begin
* Model gérant tous les commentaires
=end

class Comment < ApplicationRecord
  # table name
  self.table_name = "comments"
  # relation
  # Un commentaire appartient à un utilisateur
  belongs_to :user, :class_name => "User", :foreign_key => "id_user"
  # un commentaire appartient à un article
  belongs_to :post, :class_name => "Post", :foreign_key => "id_post"
  # verfication (validators)

end