=begin
* Model gérant tous les utilisateurs
=end

class User < ApplicationRecord
  has_many :post, :class_name => "Post", :foreign_key => "user_id"
  has_many :user, :class_name => "User", :foreign_key => "id_user"
  has_many :comment, :class_name => "Comment", :foreign_key => "id_user"
  self.table_name = "users"
end