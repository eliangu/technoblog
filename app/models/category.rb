=begin
  * Model servant à la gestion des catégories
=end

class Category < ApplicationRecord
  has_many :post, :class_name => "Post", :foreign_key => "category_id"
end