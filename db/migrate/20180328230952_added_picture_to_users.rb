class AddedPictureToUsers < ActiveRecord::Migration[5.1]
  def change
    change_table :users do |t|
      t.string :picture
    end
  end
end
