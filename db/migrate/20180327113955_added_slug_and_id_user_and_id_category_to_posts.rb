class AddedSlugAndIdUserAndIdCategoryToPosts < ActiveRecord::Migration[5.1]
  def change
    change_table :posts do |t|
      t.string :slug
      t.integer :idUser
      t.integer :idCategory
    end
  end
end
