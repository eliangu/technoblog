class AddPostedToPosts < ActiveRecord::Migration[5.1]
  def change
    add_column :posts, :posted, :integer
  end
end
