class CreatedUsersTable < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |table|
      table.string :username
      table.timestamps
    end
  end
end
