class AddedIdPostColumnToComments < ActiveRecord::Migration[5.1]
  def change
    change_table :comments do |t|
      t.integer :id_post
    end
  end
end
