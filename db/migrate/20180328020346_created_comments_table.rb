class CreatedCommentsTable < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.integer :id_user
      t.text :content
    end
  end
end
