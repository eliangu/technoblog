class AddTimestampsToComment < ActiveRecord::Migration[5.1]
  def change
    change_table :comments do |t|
      t.timestamps :default => DateTime.now
    end
  end
end
