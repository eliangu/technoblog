require 'rails_helper'

RSpec.describe CommentsController, type: :controller do

  describe("Comment #create") do


    it "Should not create comment because the post is not valid" do
      params = {content: "Bonjour", post: Post.last.id}
      session[:id] = 1
      post :create, params: params
      expect(response).to redirect_to "/"
    end


  end


end
