require 'rails_helper'

RSpec.describe PostsController, type: :controller do

  describe "get #create" do
    it "Sould not create post and return to home page" do
      session[:id] = 3
      get :create
      expect(response).to redirect_to "/"
    end

    end

  describe "post #edit" do
    it "Should not edit post and return to home page" do
      session[:id] = 3
      post :edit, params: {id: 26 }
      expect(response).to redirect_to "/"
    end
  end



  describe "post #destroy" do
    it "should destroy a post" do
      session[:id] = 1
      post :destroy, params: {id: 26}
      expect(response).to redirect_to "/profil"
    end
  end

end
