require 'rails_helper'

RSpec.describe AdminController, type: :controller do

  describe "Get #ninja" do

    it "redirect to home path" do
      get :ninja, params: {id: 1}
      expect(response).to redirect_to("/")
    end

    it 'Should protect index page and redirect to home' do

      session[:id] = 3
      get :index
      expect(response).to redirect_to("/")

    end

    it "Sould protect manage_users page and redirect_to home" do

      session[:id] = 3
      get :manage_users
      expect(response).to redirect_to("/")

    end

    it "Sould protect manage comments page and redirect_to home" do
      session[:id] = 3
      get :manage_users
      expect(response).to redirect_to "/"
    end

  end

end
