require 'rails_helper'

RSpec.describe CategoriesController, type: :controller do

  describe "GET #destroy" do

    before do
      session[:id] = 1
    end

    it "Should redirect to home page" do
      params = {id: 2}
      get :destroy, params: params
      expect(response).to redirect_to("/")
    end

    it "Should redirect to categories pages" do
      params = {id: 2, redirect: "/categories"}
      get :destroy, params: params
      expect(response).to redirect_to("/categories")
    end

  end

end
