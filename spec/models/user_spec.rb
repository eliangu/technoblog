require 'rails_helper'

RSpec.describe User, type: :model do
  context("banned") do

    let(:u) { User.find(3) }

    it "Should get not banned user" do

      expect(u.banned).to eq(0 || 1)

    end

  end
end
