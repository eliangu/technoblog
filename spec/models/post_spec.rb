require 'rails_helper'

RSpec.describe Post, type: :model do
    let(:p) { Post.new(title: "") }

    it "sould not save post" do
      expect(p.errors).to_not eq(nil)
    end

end
